#Longest Word
parameter: $word
return: the longest word in the string
conditions: if there are two o more words with the same length return the first.
            Ignore punctuation and numbers ($word will not be empty)
